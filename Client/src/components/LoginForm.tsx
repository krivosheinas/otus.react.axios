import React, { useState } from "react";
import { ILoginForm } from "../interfaces";
import axios from "axios";

export const LoginForm: React.FC = () => {

  const [login, setLogin] = useState<string>('');
  const [password, setPassword ]= useState<string>('');

  const onChangeLoginHandler = (event: React.ChangeEvent<HTMLInputElement>) =>{    
    setLogin(event.target.value)
  }


  const onChangePasswordHandler = (event: React.ChangeEvent<HTMLInputElement>) =>{   
    setPassword(event.target.value)
  }
  

  const onClickHandler = () =>{   
    let user: ILoginForm = {
      login: login,
      password: password
    }
    console.log(user)

    axios.post("http://localhost:5000/auth", user)
    .then(res => {
                   console.log('Получен ответ от сервиса аутентификации', res)
                   if (res.data.username){
                       alert(`Добро пожаловать, ${res.data.username}!`)
                   }else{
                       alert("Добро пожаловать!")
                   }
                 })
    .catch(err=> {console.error(err); alert("Аутентификация не пройдена")})
  }


  return (            
              <div className="Login">
                <h1>Вход в CMR-систему </h1>
                <div className="m-2">
                  <label>Логин</label><br></br>
                  <input className="text" onChange={onChangeLoginHandler}/>
                </div>
                <div className="m-2">
                  <label>Пароль</label><br></br>
                  <input className="text" type="password" onChange={onChangePasswordHandler}/>
                </div>
                <button className="btn btn-primary" onClick={onClickHandler}>Войти</button>
            </div>                 
        )
} 
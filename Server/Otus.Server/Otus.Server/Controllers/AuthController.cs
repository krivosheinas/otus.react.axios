﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {  
        [HttpGet]
        public ActionResult<ICollection<User>> GetAllUsers()
        {
            return LocalDataFactory.users.ToList();
        }


        [HttpPost]
        public ActionResult<string> CheckCredential(AuthModel authModel)
        {   

            var user = LocalDataFactory.users.FirstOrDefault(x => x.Login == authModel.Login && x.Password == authModel.Password);

            if (user != null)
            {
                return Ok( "{\"username\":\"" +  user.Name + "\"}");
            }
            return Unauthorized();
        }
    }
}

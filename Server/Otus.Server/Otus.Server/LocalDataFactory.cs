﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Server
{
    public static class LocalDataFactory
    {
        public static ICollection<User> users = new List<User>()
        {
            new User()
            {
                Login = "admin",
                Password = "admin",
                Name = "Администратор БД"
            },
            new User()
            {
                Login = "user",
                Password = "user",
                Name = "Пользователь системы CRM"
            },
             new User()
            {
                Login = "audit",
                Password = "audit",
                Name = "Аудитор CRM"
            }
        };
    }
}
